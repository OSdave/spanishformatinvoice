<?php

/**
 * OSdave's Spanish Invoice
 *
 * Tax Pdf Model, display Tax Base and Tax Amount in totals when "Include Tax In Grand Total" is set to "Yes"
 *
 * @author 		david.parloir@gmail.com
 * @author 		David Parloir
 * @package 	Osdave
 * @subpackage	Invoice
 * @copyright 	Copyright (c) David Parloir 2011
 * @copyright 	Licensed under the Creative Commons "Attribution-Noncommercial-Share Alike" License
 *              http://creativecommons.org/licenses/by-nc-sa/3.0/us/
 */
class Osdave_Invoice_Model_Tax_Sales_Pdf_Grandtotal extends Mage_Tax_Model_Sales_Pdf_Grandtotal
{

    protected $_taxes;

    /**
     * Check if tax amount should be included to grandtotals block
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $store = $this->getOrder()->getStore();
        $config = Mage::getSingleton('tax/config');
        if (!$config->displaySalesTaxWithGrandTotal($store)) {
            return parent::getTotalsForDisplay();
        }

        //get order's differents tax classes
        $this->_taxes = array();
        //shipping
        $shippingHasTax = Mage::getStoreConfig('tax/classes/shipping_tax_class', $this->getOrder()->getStoreId());
        if ($shippingHasTax) {
            $taxClass = Mage::helper('invoice')->getShippingTaxClass($shippingHasTax);
            $this->_taxes[$taxClass]['base'] = $this->getOrder()->getShippingInvoiced() - $this->getOrder()->getShippingDiscountAmount();
            $this->_taxes[$taxClass]['tax'] = $this->getOrder()->getShippingTaxAmount();
        }

        //products
        foreach ($this->getOrder()->getAllItems() as $item) {
            if ($item->getProductType() != 'bundle') {
                $taxClass = Mage::helper('invoice')->getTaxClass($item);
                if (!array_key_exists($taxClass, $this->_taxes)) {
                    $this->_taxes[$taxClass]['base'] = ($item->getPrice() * $item->getQtyInvoiced()) - $item->getDiscountInvoiced();
                    $this->_taxes[$taxClass]['tax'] = $item->getTaxInvoiced();
                } else {
                    $this->_taxes[$taxClass]['base'] += ($item->getPrice() * $item->getQtyInvoiced()) - $item->getDiscountInvoiced();
                    $this->_taxes[$taxClass]['tax'] += $item->getTaxInvoiced();
                }
            }
        }

        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        $amountExclTax = $this->getAmount() - $this->getSource()->getTaxAmount();
        $amountExclTax = $this->getOrder()->formatPriceTxt($amountExclTax);
//        $tax = $this->getOrder()->formatPriceTxt($this->getSource()->getTaxAmount());
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;

        $totals = array();
        $totals[] = array(
            'amount' => $this->getAmountPrefix() . $amountExclTax,
            'label' => Mage::helper('tax')->__('Grand Total (Excl. Tax)') . ':',
            'font_size' => $fontSize
        );

        foreach ($this->_taxes as $taxKey => $taxValues) {
            if ($taxValues['tax'] > 0) {
                $totals[] = array(
                    'amount' => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($taxValues['base']),
                    'label' => Mage::helper('invoice')->__('%s - Tax Base', $taxKey) . ':',
                    'font_size' => 5
                );
                $totals[] = array(
                    'amount' => $this->getAmountPrefix() . $this->getOrder()->formatPriceTxt($taxValues['tax']),
                    'label' => Mage::helper('invoice')->__('%s - Tax Value', $taxKey) . ':',
                    'font_size' => 5
                );
            }
        }

        $totals[] = array(
            'amount' => $this->getAmountPrefix() . $amount,
            'label' => Mage::helper('tax')->__('Grand Total (Incl. Tax)') . ':',
            'font_size' => $fontSize
        );

        return $totals;
    }

}
