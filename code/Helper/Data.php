<?php

/**
 * OSdave's Spanish Invoice
 *
 * Helper, gets the Tax Class's Name
 *
 * @author 		david.parloir@gmail.com
 * @author 		David Parloir
 * @package 	Osdave
 * @subpackage	Invoice
 * @copyright 	Copyright (c) David Parloir 2011
 * @copyright 	Licensed under the Creative Commons "Attribution-Noncommercial-Share Alike" License
 *              http://creativecommons.org/licenses/by-nc-sa/3.0/us/
 */
class Osdave_Invoice_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getTaxClass($item)
    {
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        $tax = Mage::getModel('tax/class')->load($product->getTaxClassId())->getClassName();

        return $tax;
    }

    public function getShippingTaxClass($shippingTax)
    {
        $tax = Mage::getModel('tax/class')->load($shippingTax)->getClassName();

        return $tax;
    }

}